//
//  ViewController.h
//  Assignment9
//
//  Created by Jason Clinger on 3/15/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import <MapKit/MapKit.h>
#import "SecondEntity.h"


@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSArray* array;
    NSString* name;
    NSString* color;
    id d;
    
    // add something more here to convert from address to coordinates
    CLGeocoder *geocoder;
}

@property (strong, nonatomic) IBOutlet UITableView *myTableView;



@end

