//
//  AddViewController.h
//  Assignment9
//
//  Created by Jason Clinger on 3/16/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ViewController.h"


@interface AddViewController : UIViewController
{
    NSString* addfirst;
    NSString* addlast;
    NSString* addaddress;
    NSString* addzip;
}

@property (weak, nonatomic) IBOutlet UITextField *textFieldFirst;

@property (weak, nonatomic) IBOutlet UITextField *textFieldLast;

@property (weak, nonatomic) IBOutlet UITextField *textFieldAddress;

@property (weak, nonatomic) IBOutlet UITextField *textFieldZip;

- (IBAction)saveBtnTouched:(UIButton *)sender;



@end
