//
//  AddViewController.m
//  Assignment9
//
//  Created by Jason Clinger on 3/16/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "AddViewController.h"

@interface AddViewController ()

@end

@implementation AddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)saveBtnTouched:(UIButton *)sender {
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObject* SecondEntity = [NSEntityDescription insertNewObjectForEntityForName:@"SecondEntity" inManagedObjectContext:ad.managedObjectContext];
    [SecondEntity setValue:self.textFieldFirst.text forKey:@"firstName"];
    [SecondEntity setValue:self.textFieldLast.text forKey:@"lastName"];
    [SecondEntity setValue:self.textFieldAddress.text forKey:@"address"];
    [SecondEntity setValue:self.textFieldZip.text forKey:@"zip"];
    NSError* error;
    if (![ad.managedObjectContext save:&error]) {
        NSLog(@"Save failed");
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });

}




@end
