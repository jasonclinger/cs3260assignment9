//
//  MapViewController.h
//  Assignment9
//
//  Created by Jason Clinger on 3/16/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "SecondEntity.h"

@interface MapViewController : UIViewController<CLLocationManagerDelegate, MKMapViewDelegate>
{
    CLLocationManager* locationManager;
    CLGeocoder* geocoder;
    double preLatCoord;
    double preLongCoord;
    NSString* addressToCoords;
}

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, retain) SecondEntity* secondEntityPerson;

@end
