//
//  MapViewController.m
//  Assignment9
//
//  Created by Jason Clinger on 3/16/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //use with mapview
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager = [[CLLocationManager alloc]init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;

    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }

    [locationManager startUpdatingLocation];

    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    
    addressToCoords = [NSString stringWithFormat:@"%@ %@", self.secondEntityPerson.address, self.secondEntityPerson.zip];
    ////call to function - convert address / zip into coords
    
    ////////////////////////////////////////////
  
    if (!geocoder)
    {
        geocoder = [[CLGeocoder alloc] init];
    }

    [geocoder geocodeAddressString:addressToCoords completionHandler:^(NSArray* placemarks, NSError* error){
        for (CLPlacemark* aPlacemark in placemarks)
        {
            // Process the placemark.
            NSString *latDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.latitude];
            NSString *lngDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.longitude];
            
            
            preLatCoord = [latDest1 doubleValue];
            preLongCoord = [lngDest1 doubleValue];
            
            NSLog(@"inside loop working");
            //break;
        }
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(preLatCoord, preLongCoord);
        
        point.coordinate = coord;
        MKCoordinateRegion region;
        region.span = MKCoordinateSpanMake(50, 50);
        
        point.title = [NSString stringWithFormat:@"%@ %@", self.secondEntityPerson.firstName, self.secondEntityPerson.lastName];
        
        point.subtitle = addressToCoords;
        
        [self.mapView addAnnotation:point];
        //[self.mapView setRegion:region animated:YES];
        //[self.mapView setRegion:region animated:YES];
        
        [self performSelector:@selector(zoomInToMyLocation)
                   withObject:nil
                   afterDelay:3]; //will zoom in after 5 seconds
    
    }];
   
    /////////////////////////////////////////////

    
//    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(preLatCoord, preLongCoord);
//    //CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(41.195064, -111.941835);
//    
//    //MKCoordinateRegion region;
//    //region.span = MKCoordinateSpanMake(800, 800);
//    
//    point.coordinate = coord;
//    
//    point.title = [NSString stringWithFormat:@"%@ %@", self.secondEntityPerson.firstName, self.secondEntityPerson.lastName];
//    
//    point.subtitle = addressToCoords;
//
//    [self.mapView addAnnotation:point];
//    //[self.mapView setRegion:region animated:YES];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
}

//use both functions with mapview
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}

// Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", [locations lastObject]);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)zoomInToMyLocation
{
    MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = preLatCoord ;
    region.center.longitude = preLongCoord;
    region.span.longitudeDelta = 0.15f;
    region.span.latitudeDelta = 0.15f;
    [self.mapView setRegion:region animated:YES];
}

@end
