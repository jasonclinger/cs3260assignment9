//
//  ViewController.m
//  Assignment9
//
//  Created by Jason Clinger on 3/15/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "ViewController.h"
#import "AddViewController.h"
#import "MapViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    self.myTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    [self.view addSubview: self.myTableView];
    
//    array = [NSMutableArray new];
//    NSLog(@"%@", array);
 
    [self.myTableView reloadData];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadData];
}


-(void) reloadData{
    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    NSError* error;
    NSFetchRequest* request = [[NSFetchRequest alloc]init];
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"SecondEntity" inManagedObjectContext:ad.managedObjectContext];
    [request setEntity:entity];
    array = [ad.managedObjectContext executeFetchRequest:request error:&error];
    NSLog(@"here");
    
    [self.myTableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return array.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SecondEntity* se = array[indexPath.row];
    [self performSegueWithIdentifier:@"toMapView" sender:se];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    SecondEntity* se = array[indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", se.firstName, se.lastName];
    //cell.textLabel.text = se.firstName;
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@", se.address, se.zip];
    //cell.detailTextLabel.text = se.address;
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier  isEqual: @"toMapView"]) {
        
        MapViewController *mapview = segue.destinationViewController;
        mapview.secondEntityPerson = sender;

    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
