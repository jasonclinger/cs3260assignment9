//
//  TestEntity+CoreDataProperties.m
//  Assignment9
//
//  Created by Jason Clinger on 3/15/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TestEntity+CoreDataProperties.h"

@implementation TestEntity (CoreDataProperties)

@dynamic name;
@dynamic secondRelationship;

@end
