//
//  SecondEntity+CoreDataProperties.h
//  Assignment9
//
//  Created by Jason Clinger on 3/17/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SecondEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface SecondEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *address;
@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSString *zip;
@property (nullable, nonatomic, retain) TestEntity *testRelationship;

@end

NS_ASSUME_NONNULL_END
