//
//  SecondEntity+CoreDataProperties.m
//  Assignment9
//
//  Created by Jason Clinger on 3/17/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SecondEntity+CoreDataProperties.h"

@implementation SecondEntity (CoreDataProperties)

@dynamic address;
@dynamic firstName;
@dynamic lastName;
@dynamic zip;
@dynamic testRelationship;

@end
