//
//  TestEntity+CoreDataProperties.h
//  Assignment9
//
//  Created by Jason Clinger on 3/15/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TestEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface TestEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSManagedObject *secondRelationship;

@end

NS_ASSUME_NONNULL_END
